package cn.leaf.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * description: 启动类
 *
 * @author Cyril
 * @version v1.0.0
 * @since 2021-05-22 12:26:54
 *
 */
@SpringBootApplication
public class TwinkleLeafElasticsearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(TwinkleLeafElasticsearchApplication.class, args);
    }

}
