package cn.leaf.elasticsearch.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * description: es配置类
 *
 * @author Cyril
 * @version v1.0.0
 * @since 2021-05-22 12:16:17
 *
 */
@Configuration
public class TwinkleLeafEsConfig {

    @Bean
    public RestHighLevelClient restHighLevelClient() {
        // 此处填写你es host的地址
        return new RestHighLevelClient(RestClient.builder(new HttpHost("your es host address", 9200)));
    }
}
